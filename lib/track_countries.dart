import 'package:covid_tracker/Models/countries_model.dart';
import 'package:covid_tracker/Utilities/urls/StateServices/state_services.dart';
import 'package:covid_tracker/details_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_spinkit/flutter_spinkit.dart';

class TrackCountries extends StatefulWidget {
  const TrackCountries({Key? key}) : super(key: key);

  @override
  State<TrackCountries> createState() => _TrackCountriesState();
}

class _TrackCountriesState extends State<TrackCountries>
    with TickerProviderStateMixin {
  late final AnimationController _controller =
      AnimationController(vsync: this, duration: Duration(seconds: 3))
        ..repeat();
  TextEditingController _searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    StateServices stateServices = StateServices();

    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('All Countries')),
      ),
      body: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: TextFormField(
              onChanged: (value) {
                setState(() {});
              },
              controller: _searchController,
              decoration: InputDecoration(
                  hintText: 'Search with the name of country',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20))),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Expanded(
            child: FutureBuilder(
              future: stateServices.getCountriesData(),
              builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
                if (!snapshot.hasData) {
                  return Expanded(
                    child: SpinKitCircle(
                      color: Colors.white,
                      size: 50,
                      controller: _controller,
                    ),
                  );
                } else {
                  return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      String name = snapshot.data![index]['country'];

                      if (_searchController.text.isEmpty) {
                        return Column(
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => DetailsScreen(
                                          name: name,
                                          image: snapshot.data![index]
                                              ['countryInfo']['flag'],
                                          totalCases: snapshot.data![index]
                                              ['cases'],
                                          totalDeaths: snapshot.data![index]
                                              ['deaths'],
                                          active: snapshot.data![index]
                                              ['active'],
                                          critical: snapshot.data![index]
                                              ['critical'],
                                          tests: snapshot.data![index]['tests'],
                                          todayRecovered: snapshot.data![index]
                                              ['todayRecovered'],
                                          totalRecovered: snapshot.data![index]
                                              ['recovered']),
                                    ));
                              },
                              child: ListTile(
                                  title: Text(snapshot.data![index]['country']),
                                  subtitle: Text("Cases:  " +
                                      snapshot.data![index]['cases']
                                          .toString()),
                                  leading: Image(
                                    height: 50,
                                    width: 50,
                                    image: NetworkImage(snapshot.data![index]
                                        ['countryInfo']['flag']),
                                  )),
                            ),
                          ],
                        );
                      } else if (name
                          .toLowerCase()
                          .contains(_searchController.text.toString())) {
                        return ListTile(
                            title: Text(snapshot.data![index]['country']),
                            subtitle: Text("Cases:  " +
                                snapshot.data![index]['cases'].toString()),
                            leading: Image(
                              height: 50,
                              width: 50,
                              image: NetworkImage(
                                  snapshot.data![index]['countryInfo']['flag']),
                            ));
                      } else {
                        return Container();
                      }
                    },
                  );
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
