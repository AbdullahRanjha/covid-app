import 'package:covid_tracker/world_stat_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class DetailsScreen extends StatefulWidget {
  String name;
  String image;
  int totalCases,
      totalDeaths,
      totalRecovered,
      active,
      critical,
      todayRecovered,
      tests;

  DetailsScreen(
      {
        required this.name,
        required this.image,
        required this.totalCases,
        required this.totalDeaths,
        required this.active,
        required this.critical,
        required this.tests,
        required this.todayRecovered,
        required this.totalRecovered});

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name),
      ),
      body: Column(
        children: [
          Stack(
            children: [
              Card(
                child: Column(
                  children: [
                    ReuseableRow(title: 'Country Name', value: widget.name.toString()),
                    ReuseableRow(title: 'Active Cases', value: widget.active.toString())
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
