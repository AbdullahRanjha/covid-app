import 'package:flutter/material.dart';

class AppUrls {
  static String baseUrl = 'https://disease.sh/v3/covid-19/';
 
  static String countriesUrl = baseUrl + 'countries';
  static String worldApi = baseUrl + 'all';
}
