import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../Models/countries_model.dart';
import '../../../Models/world_state_model.dart';
// import '../app_urls.dart';
import '../../urls/app_urls.dart';

// classorldStateServices(){}

class StateServices {
  Future<WorldStateModel> fetchWorldRecord() async {
    final response = await http.get(Uri.parse(AppUrls.worldApi));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body.toString());
      return WorldStateModel.fromJson(data);
    } else {
      throw Exception('message');
    }
  }

  // List<FlagCountriesModel> countriesFlag = [];

  Future<List<dynamic>> getCountriesData() async {
    var data;

    final response = await http.get(Uri.parse(AppUrls.countriesUrl));
    if (response.statusCode == 200) {
      data = jsonDecode(response.body.toString());
      return data;
    } else {
      throw Exception();
    }
  }

  // Future<>
}
