import 'package:covid_tracker/Models/world_state_model.dart';
import 'package:covid_tracker/Utilities/urls/StateServices/state_services.dart';
import 'package:covid_tracker/track_countries.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:pie_chart/pie_chart.dart';

class WorldStatScreen extends StatefulWidget {
  const WorldStatScreen({Key? key}) : super(key: key);

  @override
  State<WorldStatScreen> createState() => _WorldStatScreenState();
}

class _WorldStatScreenState extends State<WorldStatScreen>
    with TickerProviderStateMixin {
  late final AnimationController _controller =
      AnimationController(vsync: this, duration: Duration(seconds: 3))
        ..repeat();

  final colorsList = [
    Color.fromARGB(255, 35, 90, 186),
    Color.fromARGB(255, 150, 143, 75),
    Colors.brown
  ];

  @override
  void dispose() {
    _controller.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    StateServices _stateServices = StateServices();

    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('All World Data')),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Container(
            child: Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                Expanded(
                  child: FutureBuilder(
                    future: _stateServices.fetchWorldRecord(),
                    builder:
                        (context, AsyncSnapshot<WorldStateModel> snapshot) {
                      if (!snapshot.hasData) {
                        return Expanded(
                          child: SpinKitCircle(
                            color: Colors.white,
                            size: 50,
                            controller: _controller,
                          ),
                        );
                      } else {
                        return Column(
                          children: [
                            PieChart(
                              chartValuesOptions: ChartValuesOptions(
                                  showChartValuesInPercentage: true),
                              chartType: ChartType.ring,
                              chartRadius: 180,
                              dataMap: {
                                'Total Cases': double.parse(
                                    snapshot.data!.cases.toString()),
                                'Recovered': double.parse(
                                    snapshot.data!.recovered.toString()),
                                'Deaths': double.parse(
                                    snapshot.data!.deaths.toString()),
                              },
                              animationDuration: Duration(milliseconds: 2000),
                              colorList: colorsList,
                              legendOptions: LegendOptions(
                                  legendPosition: LegendPosition.left),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(0),
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.42,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20)),
                                child: Card(
                                    child: ListView(
                                  scrollDirection: Axis.vertical,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Column(
                                        children: [
                                          ReuseableRow(
                                              title: 'Total Cases',
                                              value: snapshot.data!.cases
                                                  .toString()),
                                          Divider(
                                            color: Colors.white,
                                          ),
                                          ReuseableRow(
                                              title: 'Today Cases',
                                              value: snapshot.data!.todayCases
                                                  .toString()),
                                          Divider(
                                            color: Colors.white,
                                          ),
                                          ReuseableRow(
                                              title: 'Deaths',
                                              value: snapshot.data!.deaths
                                                  .toString()),
                                          Divider(
                                            color: Colors.white,
                                          ),
                                          ReuseableRow(
                                              title: 'Today Deaths',
                                              value: snapshot.data!.todayDeaths
                                                  .toString()),
                                          Divider(
                                            color: Colors.white,
                                          ),
                                          ReuseableRow(
                                              title: 'Active Cases',
                                              value: snapshot.data!.active
                                                  .toString()),
                                          Divider(
                                            color: Colors.white,
                                          ),
                                          ReuseableRow(
                                              title: 'Critical Cases',
                                              value: snapshot.data!.critical
                                                  .toString()),
                                          Divider(
                                            color: Colors.white,
                                          ),
                                          ReuseableRow(
                                              title: 'Total Tests',
                                              value: snapshot.data!.tests
                                                  .toString()),
                                          Divider(
                                            color: Colors.white,
                                          ),
                                          ReuseableRow(
                                              title: 'Total Recovered',
                                              value: snapshot.data!.recovered
                                                  .toString()),
                                          Divider(
                                            color: Colors.white,
                                          ),
                                          ReuseableRow(
                                              title: 'Today Recovered',
                                              value: snapshot
                                                  .data!.todayRecovered
                                                  .toString()),
                                          Divider(
                                            color: Colors.white,
                                          ),
                                          ReuseableRow(
                                              title: 'Affected Countries',
                                              value: snapshot
                                                  .data!.affectedCountries
                                                  .toString())
                                        ],
                                      ),
                                    ),
                                  ],
                                )),
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TrackCountries(),
                                    ));
                              },
                              child: Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 40,
                                  // color: Colors.green,
                                  decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Center(
                                    child: Text(
                                      "Track Countries",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ReuseableRow extends StatelessWidget {
  ReuseableRow({Key? key, required this.title, required this.value})
      : super(key: key);

  String title, value;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(title),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(value),
        )
      ],
    );
  }
}
